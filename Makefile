REPO = local/log-collector
IMAGE = $(REPO):local

build:
	docker build -t $(IMAGE) -f Dockerfile .

run:
	@docker run \
		-it --rm --name logship \
		--hostname ct-logship \
		--env-file .env \
		-v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
		-v /etc/hostname:/tmp/hostname:ro \
		-v ./olip-files:/olip-files \
		-v ./:/host:ro \
		-v ./entrypoint.sh:/entrypoint.sh \
		-v ./push.sh:/push.sh \
		-v ./sendkey.sh:/sendkey.sh \
		$(IMAGE)

shell:
	docker exec -it logship bash



