FROM debian:12-slim


# hadolint ignore=DL3008
RUN mkdir /olip-files/ \
    && ln -s -f  /olip-files/ssh /root/.ssh \
    \
    && apt-get --quiet --quiet update \
    && apt-get install --quiet --quiet --yes --no-install-recommends \
        file \
        rsync \
        libnss-mdns \
        openssh-client \
        wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh push.sh sendkey.sh /

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/push.sh" ]
