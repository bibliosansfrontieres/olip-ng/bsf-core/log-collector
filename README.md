# Log Collector

Sends logs to some central server.

For now the container also creates the required SSH keys,
but this is subject to changes in a near future.

## Synopsis

If no SSH keys/config exists, they are created.

A cronjob sends configured files at regular interval.

### Device rename strategy

Files are sent to the remote server in a directory
named after the device's hostname.

Whenever a device is renamed, the `macswitcher` service updates the hostname.

As a result, the target directory always reflects the current deployment.

## Configuration

The configuration is done via environment variables.

The local SSH configuration part:

- `SSH_DIR`: the directory where SSH keys and config are written to / read from

The SSH Hub part:

- `SSH_HUB_HOST`: tincmaster.bsf-intranet.org
- `SSH_HUB_PORT`: 22
- `SSH_HUB_USERNAME`: ansible

The logfiles part:

- `PUSH_DELAY`: delay between runs - in minutes

Not implemented yet:

- `SSH_PUBLIC_KEY`:  a public key - as a string
- `SSH_PRIVATE_KEY`:  à private key - as a `base64` encoded string
- `SSH_CONFIG`: a SSH host config - as a `base64` encoded string
- `FILES_LIST`: a list of files (globbing allowed) - as a string

## Usage

Required bind-mounts:

- `/var/run/avahi-daemon/socket`:
    required to resolve the mDNS published by the CDN for master key retrieval
- `/etc/hostname`: used as a target path on remote Hub server
- `./olip-files`: SSH configuration storage/share
- `/`: mounted to `/host` in order to access host's filesystem

### Docker Run

```shell
docker run \
  --name log-collector \
  --env-file .env \
  -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
  -v /etc/hostname:/tmp/hostname:ro \
  -v /olip-files:/olip-files \
  -v /:/host:ro \
  offlineinternet/log-collector
```

### Docker Compose

```yaml
services:
  log-collector:
    image: offlineinternet/log-collector
    container_name: log-collector
    restart: unless-stopped
    env_file:
      - .env
    volumes:
      - /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket
      - /etc/hostname:/tmp/hostname:ro
      - /olip-files:/olip-files
      - /:/host:ro
```

## Contributing

The provided `Makefile` has `build` and `run` targets,
and bind-mounts all of the used scripts.

### Todo

- [ ] Configurable files list
- [ ] `SSH_HUB_PATH`
- [ ] use supervisord
- [ ] move the SSH management to its own tool
- [ ] `Makefile` targets: clean, internet/CDN flagfiles, ...
