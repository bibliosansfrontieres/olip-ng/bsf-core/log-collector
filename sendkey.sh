#!/bin/bash

say() {
    echo >&2 "[${0}] $*"
}

set -euo pipefail

ssh_download_master_key(){
    say "Download master SSH key from CDN..."
    wget -q "http://genericsshkey-cdn-ideascube.local/id_rsa" \
     -O /tmp/id_rsa_master
     chmod go-r /tmp/id_rsa_master
}
ssh_send_local_key_to_hub(){
    say "Send generated key to SSH Hub (${SSH_HUB_HOST})..."
    cat "${SSH_DIR}/id_rsa.pub" | ssh -i /tmp/id_rsa_master "$SSH_HUB_HOST" "cat >> .ssh/authorized_keys"
    rc=$?
    if [ "$rc" == 0 ] ; then
        say "Mark generated key as sent sucessfully..."
        sed -i -e '/^ssh-rsa/s/$/ # key_sent/' "${SSH_DIR}/id_rsa.pub"
    fi
}
ssh_delete_master_key(){
    say "Delete master SSH key..."
    rm -f /tmp/id_rsa_master
}


say "Check whether we can send our generated SSH key to ${SSH_HUB_HOST}..."
# we want both a CDN and an internet connection
# so the exchange can complete in a single run.
# Otherwise the master key stays stored and we don't want to
# risk shipping it
if [ ! -f /olip-files/network/cdn ] ; then
    say "Miss: No CDN available."
    exit 0
elif [ ! -f /olip-files/network/internet ] ; then
    say "Miss: No internet available."
    exit 0
else
    say "Do the key exchange dance..."
    ssh_download_master_key
    ssh_send_local_key_to_hub
    ssh_delete_master_key
fi
