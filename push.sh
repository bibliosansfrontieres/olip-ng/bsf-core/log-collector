#!/bin/bash

say() {
    echo >&2 "[${0}] $*"
}

set -euo pipefail

until grep -q 'key_sent' "${SSH_DIR}/id_rsa.pub" ; do
  /sendkey.sh
  sleep 60
done


say "Start sending files every $PUSH_DELAY minutes."
while true ; do

  if [ ! -f /olip-files/network/internet ] ; then
    say "Miss: no internet."
  else
    rsync -az --partial \
      /host/olip-files \
      "/host/${OLIP_VOLUME}/databases" \
      "/host/${LOGS_VOLUME}/rpi-csv-stats" \
      "${SSH_HUB_HOST}:/ansible/logs/${HOSTNAME}/"
  fi

  sleep "$(( PUSH_DELAY * 60 ))"

done
