#!/bin/bash

say() {
    echo >&2 "[${0}] $*"
}

set -euo pipefail

ssh_create_config_directory(){
    say "Create SSH configuration directory"
    mkdir -p "${SSH_DIR}"
}
ssh_write_config_file(){
    say "Write SSH configuration file"
    cat > "${SSH_DIR}/config" <<EOF
Host $SSH_HUB_HOST
    HostName                $SSH_HUB_HOST
    HostKeyAlias            $SSH_HUB_HOST
    Port                    $SSH_HUB_PORT
    User                    $SSH_HUB_USERNAME
    StrictHostKeyChecking   no
    UserKnownHostsFile      /dev/null
EOF
}
ssh_generate_keys(){
    say "Generate SSH keys pair"
    ssh-keygen -q -t rsa -b 1024 \
        -N '' -C "$HOSTNAME" \
        -f "${SSH_DIR}/id_rsa"
}

HOSTNAME="$( cat /host/etc/hostname )" ; export HOSTNAME

say "Check whether we have a SSH configuration file"
if grep -q -F 'tincmaster' "${SSH_DIR}/config" 2>/dev/null ; then
    say "SSH config found for tincmaster"
else
    say "No SSH config found for tincmaster"
    ssh_create_config_directory
    ssh_write_config_file
fi

say "Check whether we have a SSH key pair"
if file "${SSH_DIR}/id_rsa" | grep -q -F 'private key' ; then
    say "SSH key found"
else
    say "No SSH key found"
    ssh_generate_keys
fi

say "Public key: $( cat "${SSH_DIR}/id_rsa.pub" )"
say "Fingerprint: $( ssh-keygen -lf "${SSH_DIR}/id_rsa.pub" )"

exec "$@"
